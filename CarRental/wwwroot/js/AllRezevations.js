﻿
function EditRezervation(id) {
    $('#modal-wrapper').load("ViewRezervationDetails/" + id, function () {
        $('#modal-edit-rezervation').modal("show");
    });

}
function loadDataTables(url) {
    let table = "#table_id";
    var pageScrollPos = 0;
    if ($.fn.DataTable.isDataTable(table)) {
        $(table).DataTable().destroy();
        $(table).empty();
    }
    $(table).DataTable({
        "serverSide": true,
        "responsive": true,
        "ordering": true,
        "autoWidth": true,
        "processing": true,
        "searching": true,
        "footer": true,
        "fixedHeader": true,
        "lengthMenu": [10, 30, 50, 500],
        "pageLength": 10,
        "paging": true,
        "orderCellsTop": true,
        "ajax": {
            "url": url,
            "type": "POST",
            "datatype": "json",
            "dataSrc": function (json) {
                return json.data;
            },
            "complete": function (kqXHR, textStatus) {
                $($.fn.dataTable.tables(true)).DataTable()
                    .columns.adjust();
                $(".dataTables_scrollBody").scrollTop(pageScrollPos);
            }
        },
        "destroy": true,
        "retrieve": true,
        "dom": 'frt',
        "scrollY": "500px",
        "dom": 'Rlfrtip',
        "colReorder": {
            'allowReorder': true
        },
        "scrollX": true,

        "bInfo": true,
        "columns": [
            {
                title: 'Actions', data: 'IdRezervation', "render": function (data, type, full) {
                    var result = "<span><i style='color:green;cursor:pointer' onclick='EditRezervation(\"" + data + "\")' class='fas fa-edit'></i></a></span>";
                    return result;
                }
            },
            { title: "Car model", name: "ModelName", data: "ModelName", "autowidth": true },
            { title: "Car name", name: "CarName", data: "CarName", "autowidth": true },
            { title: "User first name", name: "UserFirstName", data: "UserFirstName", "autowidth": true },
            { title: "User last name", name: "UserLastName", data: "UserLastName", "autowidth": true },
            { title: "Date start rezervation", name: "StartDate", data: "StartDate", "autowidth": true },
            { title: "Date end rezervation", name: "EndDate", data: "EndDate", "autowidth": true },
            { title: "Status", name: "Status", data: "Status", "autowidth": true },
            { title: "Payment", name: "TypeOfPayment", data: "TypeOfPayment", "autowidth": true },
        ],
        "fnInitComplete": function (settings, json) { },
        "preDrawCallback": function (settings) {
            pageScrollPos = $(".dataTables_scrollBody").scrollTop();
        },
    });

}

$(document).ready(function () {
    loadDataTables('/Car/GetAllRezervations');
});
