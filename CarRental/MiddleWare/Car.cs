﻿using CarRental.MiddleWare.Interfaces;
using CarRental.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using PersitanceDb;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace CarRental.MiddleWare
{
    public class Car : ICar
    {
        private readonly DatabaseContext db;
        private readonly UserManager<PersitanceDb.Entities.User> userManager;
        private readonly SignInManager<PersitanceDb.Entities.User> signInManager;
        private readonly IEmailSender email;
        public Car(DatabaseContext databaseContext, UserManager<PersitanceDb.Entities.User> userManager, SignInManager<PersitanceDb.Entities.User> signInManager, IEmailSender email)
        {
            this.db = databaseContext;
            this.userManager = userManager;
            this.signInManager = signInManager;
            this.email = email;
        }
        public List<Models.Car> GetCars()
        {
            var cars = new List<Models.Car>();

            foreach (var model in db.CarModels)
            {
                foreach (var car in model.Car)
                {
                    var characteristics = db.CarCharacteristics.FirstOrDefault(x => x.IdCar == car.Id);
                    cars.Add(new Models.Car()
                    {
                        IdCar = car.Id,
                        /* IdCarModel = car.IdCarModel*/
                        IdCarModel = car.CarModel.Id,
                        Name = model.Name,
                        ModelName = car.Name,
                        Color = characteristics.Color,
                        PricePerDay = characteristics.PricePerDay,
                        CountDoors = characteristics.CountDoors,
                        CountSeats = characteristics.CountSeats,
                        CountLargeBags = characteristics.CountLargeBags,
                        CountSmallBags = characteristics.CountSmallBags,
                        HasAirConditioning = characteristics.HasAirConditioning,
                        GearBox = characteristics.Gearbox,
                        Status = characteristics.Status,
                        Km = characteristics.Km,
                        Image = characteristics.Img != string.Empty ? characteristics.Img : string.Empty,
                        Rating = characteristics.TotalVotes != 0 ? characteristics.TotalVotesSum / characteristics.TotalVotes : 0
                    });
                }
            }
            return cars;
        }

        public bool ListContainItems(List<Item> list, string item)
        {
            if (list.Count() == 0) return false;
            foreach (var l in list)
            {
                if (l.Name == item)
                {
                    return true;
                }
            }
            return false;
        }
        public CarFilters GetFilters()
        {
            var carFilters = new Models.CarFilters();
            carFilters.Models = db.CarModels.Select(x => new Item { Name = x.Name, Selected = false }).ToList();
            foreach (var chr in db.CarCharacteristics)
            {
                if (!ListContainItems(carFilters.Colors, chr.Color))
                {
                    carFilters.Colors.Add(new Item { Name = chr.Color, Selected = false });
                }

                if (!ListContainItems(carFilters.Seats, chr.CountSeats.ToString()))
                {
                    carFilters.Seats.Add(new Item { Name = chr.CountSeats.ToString(), Selected = false });
                }

                if (!ListContainItems(carFilters.Doors, chr.CountDoors.ToString()))
                {
                    carFilters.Doors.Add(new Item { Name = chr.CountDoors.ToString(), Selected = false });
                }

                if (!ListContainItems(carFilters.CountLargeBags, chr.CountLargeBags.ToString()))
                {
                    carFilters.CountLargeBags.Add(new Item { Name = chr.CountLargeBags.ToString(), Selected = false });
                }

                if (!ListContainItems(carFilters.CountSmallBags, chr.CountSmallBags.ToString()))
                {
                    carFilters.CountSmallBags.Add(new Item { Name = chr.CountSmallBags.ToString(), Selected = false });
                }
                if (!ListContainItems(carFilters.GearTypes, chr.Gearbox))
                {
                    carFilters.GearTypes.Add(new Item { Name = chr.Gearbox, Selected = false });
                }
            }
            return carFilters;
        }
        public List<Models.Car> GetFiltered(Models.CarFilters filters)
        {

            var cars = new List<Models.Car>();
            var colorFilter = filters.Colors.Where(x => x.Selected == true).Select(x => x.Name).ToList();
            var seatsFilter = filters.Seats.Where(x => x.Selected == true).Select(x => Int32.Parse(x.Name)).ToList();
            var modelsFilter = filters.Models.Where(x => x.Selected == true).Select(x => x.Name).ToList();
            var doorFilter = filters.Doors.Where(x => x.Selected == true).Select(x => Int32.Parse(x.Name)).ToList();
            var largeBagsFilter = filters.CountLargeBags.Where(x => x.Selected == true).Select(x => Int32.Parse(x.Name)).ToList();
            var smallBagsFilter = filters.CountSmallBags.Where(x => x.Selected == true).Select(x => Int32.Parse(x.Name)).ToList();
            var gearTypeFilter = filters.GearTypes.Where(x => x.Selected == true).Select(x => x.Name).ToList();

            foreach (var model in db.CarModels)
            {
                foreach (var car in model.Car)
                {
                    var characteristics = db.CarCharacteristics.FirstOrDefault(x => x.IdCar == car.Id);
                    if (colorFilter.Count() != 0 && !colorFilter.Contains(characteristics.Color)) continue;
                    if (seatsFilter.Count() != 0 && !seatsFilter.Contains(characteristics.CountSeats)) continue;
                    if (doorFilter.Count() != 0 && !doorFilter.Contains(characteristics.CountDoors)) continue;
                    if (largeBagsFilter.Count() != 0 && !largeBagsFilter.Contains(characteristics.CountLargeBags)) continue;
                    if (smallBagsFilter.Count() != 0 && !smallBagsFilter.Contains(characteristics.CountSmallBags)) continue;
                    if (gearTypeFilter.Count() != 0 && !gearTypeFilter.Contains(characteristics.Gearbox)) continue;
                    if (modelsFilter.Count() != 0 && !modelsFilter.Contains(model.Name)) continue;

                    cars.Add(new Models.Car()
                    {
                        IdCar = car.Id,
                        //IdCarModel = car.IdCarModel,
                        IdCarModel = car.CarModel.Id,
                        ModelName = model.Name,
                        Name = car.Name,
                        Color = characteristics.Color,
                        PricePerDay = characteristics.PricePerDay,
                        CountDoors = characteristics.CountDoors,
                        CountSeats = characteristics.CountSeats,
                        CountLargeBags = characteristics.CountLargeBags,
                        CountSmallBags = characteristics.CountSmallBags,
                        HasAirConditioning = characteristics.HasAirConditioning,
                        GearBox = characteristics.Gearbox,
                        Status = characteristics.Status,
                        Km = characteristics.Km,
                        Image = characteristics.Img != string.Empty ? characteristics.Img : string.Empty
                    });
                }
            }
            return cars;
        }
        public List<PersitanceDb.Entities.CarModels> GetModels()
        {
            return db.CarModels.ToList();
        }
        public List<string> GetGearBox()
        {
            return db.CarCharacteristics.Select(x => x.Gearbox).Distinct().ToList();
        }
        public bool Add(Models.NewCar car)
        {
            using (IDbContextTransaction transaction = db.Database.BeginTransaction())
            {
                try
                {
                    var model = db.CarModels.FirstOrDefault(x => x.Id == car.IdCarModel);
                    if (model == null)
                    {
                        model = new PersitanceDb.Entities.CarModels()
                        {
                            Name = car.ModelName
                        };
                        db.CarModels.Add(model);
                        db.SaveChanges();
                        db.Entry(model).Reload();
                    }
                    var newCar = new PersitanceDb.Entities.Car()
                    {
                        Name = car.Name,
                        CarModel = model
                    };
                    db.Cars.Add(newCar);
                    db.Entry(newCar).Reload();

                    model.Car.Add(newCar);
                    db.SaveChanges();


                    var fileName = newCar.Id + Path.GetExtension(Path.GetFileName(car.Image));


                    var sourcePath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot\\images\\cars", "temp.jpg");
                    var newName = newCar.Id + Path.GetExtension(Path.GetFileName(car.Image));
                    var directory = Path.GetDirectoryName(sourcePath);

                    var destinationPath = Path.Combine(directory, newName);

                    File.Move(sourcePath, destinationPath);

                    var characteristics = new PersitanceDb.Entities.CarCharacteristics()
                    {
                        IdCar = newCar.Id,
                        Color = car.Color,
                        PricePerDay = car.PricePerDay,
                        CountDoors = car.CountDoors,
                        CountSeats = car.CountSeats,
                        CountLargeBags = car.CountLargeBags,
                        CountSmallBags = car.CountSmallBags,
                        HasAirConditioning = car.HasAirConditioning,
                        Gearbox = car.Gearbox,
                        Status = car.Status,
                        Km = car.Km,
                        Img = fileName

                    };
                    db.CarCharacteristics.Add(characteristics);
                    db.SaveChanges();
                    transaction.Commit();
                }
                catch (Exception exception)
                {
                    transaction.Rollback();
                    return false;
                }
                return true;
            }
        }
        public Models.NewCar GetById(int id)
        {
            var car = db.Cars.FirstOrDefault(x => x.Id == id);
            var characteristics = db.CarCharacteristics.FirstOrDefault(x => x.IdCar == car.Id);
            var model = new NewCar()
            {
                Id = car.Id,
                //IdCarModel = car.IdCarModel,
                IdCarModel = car.CarModel.Id,
                ModelName = car.CarModel.Name,
                Name = car.Name,
                Color = characteristics.Color,
                PricePerDay = characteristics.PricePerDay,
                CountDoors = characteristics.CountDoors,
                CountSeats = characteristics.CountSeats,
                CountLargeBags = characteristics.CountLargeBags,
                CountSmallBags = characteristics.CountSmallBags,
                HasAirConditioning = characteristics.HasAirConditioning,
                Gearbox = characteristics.Gearbox,
                Status = characteristics.Status,
                Km = characteristics.Km,
                Image = characteristics.Img != string.Empty ? characteristics.Img : string.Empty
            };
            return model;
        }
        public bool Edit(Models.NewCar model)
        {
            using (IDbContextTransaction transaction = db.Database.BeginTransaction())
            {
                try
                {
                    var car = db.Cars.FirstOrDefault(x => x.Id == model.Id);
                    var oldMarca = db.CarModels.FirstOrDefault(x => x.Id == car.CarModel.Id);

                    if (oldMarca.Id != model.IdCarModel)
                    {
                        var newMarca = db.CarModels.FirstOrDefault(x => x.Id == model.IdCarModel);
                        if (newMarca == null)
                        {
                            newMarca = new PersitanceDb.Entities.CarModels()
                            {
                                Name = model.ModelName,
                                Car = new List<PersitanceDb.Entities.Car>()
                            };
                            db.CarModels.Add(newMarca);
                            db.SaveChanges();
                            db.Entry(newMarca).Reload();
                        }

                        car.CarModel = newMarca;
                        car.CarModel = newMarca;
                        db.SaveChanges();

                        if (oldMarca.Car.Count() == 0)
                        {
                            db.CarModels.Remove(oldMarca);
                            db.SaveChanges();
                        }
                    }

                    var test = db.CarCharacteristics.ToList();
                    var oldCharacteristics = db.CarCharacteristics.Where(x => x.IdCar == model.Id).FirstOrDefault();
                    car.Name = model.Name;
                    oldCharacteristics.Img = model.Image;
                    oldCharacteristics.Km = model.Km;
                    oldCharacteristics.CountLargeBags = model.CountLargeBags;
                    oldCharacteristics.CountSmallBags = model.CountSmallBags;
                    oldCharacteristics.CountSeats = model.CountSeats;
                    oldCharacteristics.CountDoors = model.CountDoors;
                    oldCharacteristics.PricePerDay = model.PricePerDay;
                    oldCharacteristics.Status = model.Status;
                    oldCharacteristics.Gearbox = model.Gearbox;
                    if (model.Image.Contains("temp"))
                    {
                        var fileName = car.Id + Path.GetExtension(Path.GetFileName(oldCharacteristics.Img));

                        if (File.Exists(Path.Combine(Directory.GetCurrentDirectory(), "wwwroot\\images\\cars", fileName)))
                        {
                            File.Delete(Path.Combine(Directory.GetCurrentDirectory(), "wwwroot\\images\\cars", fileName));
                        }


                        var sourcePath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot\\images\\cars", "temp.jpg");
                        var newName = car.Id + Path.GetExtension(Path.GetFileName(oldCharacteristics.Img));
                        var directory = Path.GetDirectoryName(sourcePath);

                        var destinationPath = Path.Combine(directory, newName);

                        File.Move(sourcePath, destinationPath);

                        oldCharacteristics.Img = fileName;
                    }
                    db.Entry(oldCharacteristics).State = EntityState.Modified;
                    db.SaveChanges();
                    transaction.Commit();

                }
                catch (Exception exception)
                {
                    transaction.Rollback();
                    return false;
                }
            }

            return true;
        }
        public void DeleteById(int id)
        {
            using (IDbContextTransaction transaction = db.Database.BeginTransaction())
            {
                try
                {
                    var characteristics = db.CarCharacteristics.FirstOrDefault(x => x.IdCar == id);
                    db.CarCharacteristics.Remove(characteristics);
                    var car = db.Cars.FirstOrDefault(x => x.Id == id);
                    db.Cars.Remove(car);
                    var model = db.CarModels.FirstOrDefault(x => x.Id == car.CarModel.Id);
                    if (model.Car.Count == 0)
                    {
                        db.CarModels.Remove(model);
                    }
                    db.SaveChanges();
                    transaction.Commit();
                }
                catch (Exception exception)
                {
                    transaction.Rollback();
                }
            }
        }
        public Models.ViewDeal CastNewCarToViewDeal(Models.NewCar car)
        {
            return new ViewDeal()
            {
                Id = car.Id,
                IdCarModel = car.IdCarModel,
                Name = car.Name,
                ModelName = car.ModelName,
                Color = car.Color,
                Gearbox = car.Gearbox,
                PricePerDay = car.PricePerDay,
                CountSmallBags = car.CountSmallBags,
                CountLargeBags = car.CountLargeBags,
                CountSeats = car.CountSeats,
                CountDoors = car.CountDoors,
                Image = car.Image,
                Km = car.Km,
                HasAirConditioning = car.HasAirConditioning,
                Status = car.Status
            };
        }
        public void Rezerv(Models.ViewDeal viewDeal)
        {
            using (IDbContextTransaction transaction = db.Database.BeginTransaction())
            {
                try
                {
                    var user = userManager.FindByIdAsync(viewDeal.IdUser).Result;
                    var car = db.Cars.FirstOrDefault(x => x.Id == viewDeal.Id);

                    var newRez = new PersitanceDb.Entities.Rezervation()
                    {
                        IdCar = car.Id,
                        Car = car,
                        IdUser = user.Id,
                        User = user,
                        Status = Helpers.CarStatus.Reserved.ToString(),
                        DateStart = viewDeal.StartDate,
                        DateEnd = viewDeal.EndDate,
                        TypeOfPayment = viewDeal.TypeOfPayment
                    };
                    db.Rezervations.Add(newRez);

                    var chrs = db.CarCharacteristics.FirstOrDefault(x => x.IdCar == viewDeal.Id);
                    chrs.Status = Helpers.CarStatus.Reserved.ToString();
                    db.Entry(chrs).State = EntityState.Modified;
                    db.SaveChanges();
                    transaction.Commit();
                }
                catch (Exception exception)
                {
                    transaction.Rollback();
                }
            }
        }

        public List<TableRezervation> GetAllRezervations(string col = "Status", string dir = "asc", string search = "")
        {
            col = col == string.Empty ? "Status" : col;
            List<TableRezervation> rezs = new List<TableRezervation>();
            foreach (var rez in db.Rezervations)
            {
                var user = db.User.FirstOrDefault(x => x.Id == rez.IdUser);
                var tableRez = new TableRezervation()
                {
                    IdRezervation = rez.Id,
                    UserFirstName = user.FirstName,
                    UserLastName = user.LastName,
                    StartDate = rez.DateStart,
                    EndDate = rez.DateEnd,
                    Status = rez.Status,
                    TypeOfPayment = rez.TypeOfPayment
                };
                var car = db.Cars.FirstOrDefault(x => x.Id == rez.IdCar);
                tableRez.CarName = car.Name;
                tableRez.ModelName = car.CarModel.Name;

                if (search != string.Empty)
                {
                    if (tableRez.StartDate.ToString().Contains(search) || tableRez.EndDate.ToString().Contains(search) || tableRez.Status.Contains(search) || tableRez.TypeOfPayment.Contains(search) || tableRez.CarName.Contains(search) || tableRez.ModelName.Contains(search))
                    {
                        rezs.Add(tableRez);
                    }
                }
                else
                {
                    rezs.Add(tableRez);
                }
            }


            if (dir == "asc")
            {
                return rezs.OrderBy(x => x.GetType().GetProperty(col).GetValue(x)).ToList();
            }
            else
            {
                return rezs.OrderByDescending(x => x.GetType().GetProperty(col).GetValue(x)).ToList();
            }
        }

        public ViewRezervationDetails GetRezervationDetailsById(int id)
        {
            var rez = db.Rezervations.FirstOrDefault(x => x.Id == id);
            var user = db.User.FirstOrDefault(x => x.Id == rez.IdUser);
            var car = db.Cars.FirstOrDefault(x => x.Id == rez.IdCar);
            var chrs = db.CarCharacteristics.FirstOrDefault(x => x.IdCar == car.Id);
            var newRezervationDetails = new Models.ViewRezervationDetails()
            {
                IdRezervation = rez.Id,
                Id = rez.IdCar,
                IdUser = rez.IdUser,
                IdCarModel = car.CarModel.Id,
                UserFirstName = user.FirstName,
                UserLastName = user.LastName,
                Name = car.Name,
                ModelName = car.CarModel.Name,
                Color = chrs.Color,
                PricePerDay = chrs.PricePerDay,
                CountDoors = chrs.CountDoors,
                CountSeats = chrs.CountSeats,
                CountLargeBags = chrs.CountLargeBags,
                CountSmallBags = chrs.CountSmallBags,
                HasAirConditioning = chrs.HasAirConditioning,
                Gearbox = chrs.Gearbox,
                Status = chrs.Status,
                Km = chrs.Km,
                Image = chrs.Img,
                TypeOfPayment = rez.TypeOfPayment,
                Voted = rez.Voted,
                Rating = chrs.TotalVotes != 0.0f ? chrs.TotalVotesSum / chrs.TotalVotes : 0.0f,
                StartDate = rez.DateStart,
                EndDate = rez.DateEnd
            };
            return newRezervationDetails;
        }
        public void CarReturned(ViewRezervationDetails model)
        {
            using (var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    var rez = db.Rezervations.FirstOrDefault(x => x.Id == model.IdRezervation);
                    rez.Status = Helpers.CarStatus.Returned.ToString();
                    db.Entry(rez).State = EntityState.Modified;
                    db.SaveChanges();


                    var chrs = db.CarCharacteristics.FirstOrDefault(x => x.IdCar == rez.IdCar);
                    chrs.Status = Helpers.CarStatus.Available.ToString();
                    chrs.Km = model.Km;
                    db.Entry(chrs).State = EntityState.Modified;
                    db.SaveChanges();
                    transaction.Commit();

                    var to = db.User.FirstOrDefault(x => x.Id == rez.IdUser).Email;
                    var from = "marcupalii@gmail.com";
                    var subject = "Receipt Car rental";
                    var res = email.SendEmail(to, from, subject, rez.Id);

                }
                catch
                {
                    transaction.Rollback();
                }
            }

        }
        public List<TableHistory> GetAllHistory(string col = "Status", string dir = "asc", string search = "", string idUser = "")
        {
            col = col == string.Empty ? "Status" : col;
            List<TableHistory> histories = new List<TableHistory>();
            var user = db.User.FirstOrDefault(x => x.Id == idUser);

            foreach (var rez in user.Rezervation)
            {

                var tableRez = new TableHistory()
                {
                    IdRezervation = rez.Id,
                    StartDate = rez.DateStart,
                    EndDate = rez.DateEnd,
                    Status = rez.Status,
                    TypeOfPayment = rez.TypeOfPayment
                };
                var car = db.Cars.FirstOrDefault(x => x.Id == rez.IdCar);
                tableRez.CarName = car.Name;
                tableRez.ModelName = car.CarModel.Name;

                if (search != string.Empty)
                {
                    if (tableRez.StartDate.ToString().Contains(search) || tableRez.EndDate.ToString().Contains(search) || tableRez.Status.Contains(search) || tableRez.TypeOfPayment.Contains(search) || tableRez.CarName.Contains(search) || tableRez.ModelName.Contains(search))
                    {
                        histories.Add(tableRez);
                    }
                }
                else
                {
                    histories.Add(tableRez);
                }
            }


            if (dir == "asc")
            {
                return histories.OrderBy(x => x.GetType().GetProperty(col).GetValue(x)).ToList();
            }
            else
            {
                return histories.OrderByDescending(x => x.GetType().GetProperty(col).GetValue(x)).ToList();
            }
        }
        public void RateCar(float rate, int idRezervation)
        {
            using (var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    var rez = db.Rezervations.FirstOrDefault(x => x.Id == idRezervation);
                    rez.Voted = true;
                    db.Entry(rez).State = EntityState.Modified;

                    var chrs = db.CarCharacteristics.FirstOrDefault(x => x.IdCar == rez.IdCar);
                    chrs.TotalVotes += 1;
                    chrs.TotalVotesSum += rate;

                    db.Entry(chrs).State = EntityState.Modified;

                    db.SaveChanges();
                    transaction.Commit();
                }
                catch
                {
                    transaction.Rollback();
                }
            }
        }
    }
}
