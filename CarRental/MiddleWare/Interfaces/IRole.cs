﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;

namespace CarRental.MiddleWare.Interfaces
{
    public interface IRole
    {
        public List<IdentityRole> getRoles();
        public List<IdentityRole> GetUserRoles(PersitanceDb.Entities.User utilizator);
        public Models.Role IdentityRoleToRole(IdentityRole identityRole);
    }
}
