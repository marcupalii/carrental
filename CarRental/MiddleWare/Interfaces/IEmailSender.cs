﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarRental.MiddleWare.Interfaces
{
    public interface IEmailSender
    {
        public Task Execute(string email);
        public string SendEmail(string to, string from, string subject, int idRezervare);
    }
}
