﻿using CarRental.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarRental.MiddleWare.Interfaces
{
    public interface IUser
    {
        public string Edit(Models.EditUser user);
        public List<TableUser> GetAll(string col = "FirstName", string dir = "asc", string search = "");
        public EditUser GetById(string id);
        public void Delete(string id);
        public PersitanceDb.Entities.User GetByTokens(string token1, string token2);
        public void UpdatePassword(RetypePassword retypePassword);
        public EditUser UtilizatorToEditUser(PersitanceDb.Entities.User utilizator);
        public PersitanceDb.Entities.User EditUserToUtilizator(EditUser editUser);
    }
}
