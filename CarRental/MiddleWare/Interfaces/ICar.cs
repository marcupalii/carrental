﻿using CarRental.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarRental.MiddleWare.Interfaces
{
    public interface ICar
    {
        public List<Models.Car> GetCars();
        public List<Models.Car> GetFiltered(Models.CarFilters filters);
        public Models.CarFilters GetFilters();
        public List<PersitanceDb.Entities.CarModels> GetModels();
        public List<string> GetGearBox();
        public bool Add(Models.NewCar car);
        public bool Edit(Models.NewCar car);
        public Models.NewCar GetById(int id);
        public void DeleteById(int id);
        public Models.ViewDeal CastNewCarToViewDeal(Models.NewCar car);
        public void Rezerv(Models.ViewDeal viewDeal);
        public List<TableRezervation> GetAllRezervations(string col = "FirstName", string dir = "asc", string search = "");
        public List<TableHistory> GetAllHistory(string col = "Status", string dir = "asc", string search = "", string idUser = "");

        public ViewRezervationDetails GetRezervationDetailsById(int id);
        public void CarReturned(ViewRezervationDetails model);
        public void RateCar(float rate, int idRezervation);

    }
}
