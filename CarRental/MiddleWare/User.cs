﻿using CarRental.MiddleWare.Interfaces;
using CarRental.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using PersitanceDb;
using PersitanceDb.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarRental.MiddleWare
{
    public class User : IUser
    {
        private readonly DatabaseContext db;
        private readonly IRole roleMiddleware;
        private readonly UserManager<PersitanceDb.Entities.User> userManager;
        private readonly RoleManager<IdentityRole> roleManager;
        public User(DatabaseContext databaseContext, IRole roleMiddleware, UserManager<PersitanceDb.Entities.User> userManager, RoleManager<IdentityRole> roleManager)
        {
            this.db = databaseContext;
            this.roleMiddleware = roleMiddleware;
            this.userManager = userManager;
            this.roleManager = roleManager;
        }
        public List<TableUser> GetAll(string col, string dir = "asc", string search = "")
        {
            col = col == string.Empty ? "FirstName" : col;
            List<TableUser> users = new List<TableUser>();
            if (search != string.Empty)
            {
                foreach (var user in db.User)
                {
                    if (user.LastName.Contains(search) || user.FirstName.Contains(search) || user.Address.Contains(search) || user.Email.Contains(search) || userManager.GetRolesAsync(user).Result.Contains(search))
                    {
                        users.Add(new TableUser()
                        {
                            Id = user.Id,
                            FirstName = user.FirstName,
                            LastName = user.LastName,
                            Address = user.Address,
                            Email = user.Email,
                            Roles = string.Join(", ", userManager.GetRolesAsync(user).Result.ToArray())
                        });
                    }
                }
            }
            else
            {
                users = db.User.Select(user => new TableUser()
                {
                    Id = user.Id,
                    FirstName = user.FirstName,
                    LastName = user.LastName,
                    Address = user.Address,
                    Email = user.Email,
                    Roles = string.Join(", ", userManager.GetRolesAsync(user).Result.ToArray())
                }).ToList();
            }
            if (dir == "asc")
            {
                return users.OrderBy(x => x.GetType().GetProperty(col).GetValue(x)).ToList();
            }
            else
            {
                return users.OrderByDescending(x => x.GetType().GetProperty(col).GetValue(x)).ToList();
            }

        }
        public EditUser GetById(string id)
        {
            return UtilizatorToEditUser(db.User.FirstOrDefault(x => x.Id == id));
        }
        public EditUser UtilizatorToEditUser(PersitanceDb.Entities.User utilizator)
        {
            var allRoles = roleMiddleware.getRoles();
            var roles = roleMiddleware.GetUserRoles(utilizator);
            return new EditUser()
            {
                Id = utilizator.Id,
                LastName = utilizator.LastName,
                FirstName = utilizator.FirstName,
                Email = utilizator.Email,
                Address = utilizator.Address,
                Roles = roleMiddleware.GetUserRoles(utilizator),
                AllRoles = roleMiddleware.getRoles().Select(x => roleMiddleware.IdentityRoleToRole(x)).ToList()
            };
        }
        public PersitanceDb.Entities.User EditUserToUtilizator(EditUser editUser)
        {
            return new PersitanceDb.Entities.User()
            {
                Id = editUser.Id,
                Email = editUser.Email,
            };
        }
        public string Edit(Models.EditUser user)
        {
            using (IDbContextTransaction transaction = db.Database.BeginTransaction())
            {
                try
                {
                    var userEntity = db.User.FirstOrDefault(x => x.Id == user.Id);
                    userEntity.Address = user.Address;
                    userEntity.Email = user.Email;
                    userEntity.FirstName = user.FirstName;
                    userEntity.LastName = user.LastName;

                    foreach (var role in roleMiddleware.GetUserRoles(EditUserToUtilizator(user)))
                    {
                        if (!user.RolesId.Contains(role.Id))
                        {
                            var userRole = db.UserRoles.FirstOrDefault(x => x.UserId == user.Id && x.RoleId == role.Id);
                            db.UserRoles.Remove(userRole);
                        }
                    }

                    db.SaveChanges();
                    db.Entry(userEntity).Reload();

                    foreach (var roleId in user.RolesId)
                    {
                        if (!roleMiddleware.GetUserRoles(userEntity).Select(x => x.Id).Contains(roleId))
                        {
                            var role = db.Roles.FirstOrDefault(x => x.Id == roleId);
                            var roleAsigned = new IdentityUserRole<string>()
                            {
                                UserId = userEntity.Id,
                                RoleId = role.Id
                            };
                            db.UserRoles.Add(roleAsigned);

                        }
                    }
                    db.SaveChanges();
                    transaction.Commit();
                    return user.Id;
                }
                catch
                {
                    transaction.Rollback();
                    return user.Id;
                }
            }

        }
        public void Delete(string id)
        {
            using (IDbContextTransaction transaction = db.Database.BeginTransaction())
            {
                try
                {
                    var user = db.User.FirstOrDefault(x => x.Id == id);
                    var asignedRole = db.UserRoles.FirstOrDefault(x => x.UserId == id);
                    db.UserRoles.Remove(asignedRole);
                    db.User.Remove(user);
                    db.SaveChanges();
                    transaction.Commit();
                }
                catch
                {
                    transaction.Rollback();
                }
            }

        }
        public PersitanceDb.Entities.User GetByTokens(string token1, string token2)
        {
            return db.User.FirstOrDefault(x => x.Id == token1 && x.PasswordHash == token2);
        }
        public void UpdatePassword(RetypePassword retypePassword)
        {
            using (IDbContextTransaction transaction = db.Database.BeginTransaction())
            {
                try
                {
                    var user = db.User.FirstOrDefault(x => x.Id == retypePassword.Id);
                    user.PasswordHash = userManager.PasswordHasher.HashPassword(user, retypePassword.Password);
                    db.Entry(user).State = EntityState.Modified;
                    db.SaveChanges();
                    transaction.Commit();
                }
                catch
                {
                    transaction.Rollback();
                }
            }
        }
    }
}
