﻿using System;
using System.Linq;
using System.Threading.Tasks;
using CarRental.MiddleWare.Interfaces;
using CarRental.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
namespace CarRental.Controllers
{
    public class AccountController : Controller
    {
        private Microsoft.Extensions.Logging.ILogger logger;
        private readonly UserManager<PersitanceDb.Entities.User> userManager;
        private readonly SignInManager<PersitanceDb.Entities.User> signInManager;
        private readonly IUser userMiddleWare;
        private readonly IEmailSender emailSender;
        private readonly ICar carMiddleWare;
        private readonly PersitanceDb.DatabaseContext db;
        public AccountController(ILogger<AccountController> logger, UserManager<PersitanceDb.Entities.User> userManager, SignInManager<PersitanceDb.Entities.User> signInManager, IUser userMiddleWare, IEmailSender emailSender, ICar carMiddleWare, PersitanceDb.DatabaseContext db)
        {
            this.logger = logger;
            this.userManager = userManager;
            this.signInManager = signInManager;
            this.userMiddleWare = userMiddleWare;
            this.emailSender = emailSender;
            this.carMiddleWare = carMiddleWare;
            this.db = db;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Logout()
        {

            var user = HttpContext.User;
            await signInManager.SignOutAsync();


            logger.LogInformation("User logged out.");
            return RedirectToAction(nameof(HomeController.Index), "Home");
        }

        [HttpGet]
        public IActionResult Login()
        {
            return View("Login");
        }

        [HttpGet]
        [Authorize]
        public IActionResult Profile()
        {
            var currentUser = userManager.GetUserAsync(HttpContext.User).Result;
            var roles = userManager.GetRolesAsync(currentUser).Result;

            var profile = new Models.Profile()
            {
                LastName = currentUser.LastName,
                FirstName = currentUser.FirstName,
                Address = currentUser.Address,
                Email = currentUser.Email,
                Roles = string.Join(", ", roles)
            };
            return View(profile);
        }
        [HttpGet]
        public IActionResult Register()
        {
            return View("Register");
        }
        [HttpPost]
        public async Task<ActionResult> Register(NewUser user)
        {
            if (ModelState.IsValid)
            {
                var newUser = new PersitanceDb.Entities.User()
                {
                    FirstName = user.FirstName,
                    LastName = user.LastName,
                    Email = user.Email,
                    Address = user.Address,
                    UserName = user.Email
                };

                var result = await userManager.CreateAsync(newUser, user.Password);
                if (result.Succeeded)
                {
                    await signInManager.SignInAsync(newUser, true);
                    return RedirectToAction("Profile");
                }
            }

            return View("Register");

        }


        [HttpPost]
        public async Task<IActionResult> Login(LoginViewModel model)
        {
            if (ModelState.IsValid)
            {
                var result = await signInManager.PasswordSignInAsync(model.Email, model.Password, model.RememberMe, false);

                if (result.Succeeded)
                {
                    return RedirectToAction("Profile");
                }

                ModelState.AddModelError(string.Empty, "Invalid Login Attempt");
            }

            return View("Login", model);
        }
        [HttpGet]
        public async Task<IActionResult> ResetPassword()
        {
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> ResetPassword(ResetPassword resetPassword)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    emailSender.Execute(resetPassword.Email).Wait();
                    return RedirectToAction("Login");
                }
                catch (Exception exception)
                {

                }
            }
            return View(resetPassword);
        }

        public IActionResult RetypePassword(string token1, string token2)
        {
            var user = userMiddleWare.GetByTokens(token1, token2);
            if (user != null)
            {
                var model = new RetypePassword()
                {
                    Id = user.Id,
                    Email = user.Email
                };
                return View(model);
            }
            else
                return RedirectToAction("Login");
        }
        [HttpPost]
        public async Task<IActionResult> RetypePassword(RetypePassword retypePassword)
        {
            if (ModelState.IsValid)
            {
                userMiddleWare.UpdatePassword(retypePassword);
                return RedirectToAction("Login");
            }
            else
                return View(retypePassword);
        }
        [HttpGet]
        public IActionResult History()
        {
            return View();
        }
        [HttpPost]
        [Authorize]
        public IActionResult GetAllHistory()
        {
            var currentUser = userManager.GetUserAsync(HttpContext.User).Result;

            var length = Convert.ToInt32(Request.Form["length"]);
            var start = Convert.ToInt32(Request.Form["start"]);
            var col = Request.Form[$"columns[{Request.Form["order[0][column]"]}][name]"];
            var search = Request.Form["search[value]"];
            var dir = Request.Form["order[0][dir]"];
            var rezs = carMiddleWare.GetAllHistory(col, dir, search, currentUser.Id).ToList();
            return Json(new
            {
                data = rezs.Skip(start).Take(length),
                recordsTotal = rezs.Count(),
                recordsFiltered = rezs.Count()
            });
        }

        [HttpGet]
        [Authorize]
        public IActionResult ViewHistoryDetails(int id)
        {
            var rez = carMiddleWare.GetRezervationDetailsById(id);
            return PartialView("_ViewHistoryDetails", rez);
        }

        [HttpGet]
        public IActionResult Vote(float rate, int idRezervation)
        {
            carMiddleWare.RateCar(rate, idRezervation);
            return Json("success");
        }
    }
}