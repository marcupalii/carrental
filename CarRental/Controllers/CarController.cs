﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using CarRental.MiddleWare.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace CarRental.Controllers
{
    public class CarController : Controller
    {
        private readonly ICar carMiddleWare;

        private readonly UserManager<PersitanceDb.Entities.User> userManager;
        private readonly SignInManager<PersitanceDb.Entities.User> signInManager;
        private readonly RoleManager<IdentityRole> roleManager;
        private readonly IEmailSender email;


        public CarController(ICar carMiddleWare, UserManager<PersitanceDb.Entities.User> userManager, SignInManager<PersitanceDb.Entities.User> signInManager, RoleManager<IdentityRole> roleManager, IEmailSender email)
        {
            this.carMiddleWare = carMiddleWare;
            this.userManager = this.userManager;
            this.signInManager = this.signInManager;
            this.email = email;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public IActionResult Filters()
        {
            var filters = carMiddleWare.GetFilters();
            return PartialView("_SideFilters", filters);
        }
        [HttpGet]
        public IActionResult GetCars()
        {

            var cars = carMiddleWare.GetCars().Where(x => x.Status == Helpers.CarStatus.Available.ToString()).ToList();
            return PartialView("_Cars", cars);
        }
        [HttpPost]
        public IActionResult Filters(Models.CarFilters filters)
        {
            var cars = carMiddleWare.GetFiltered(filters).Where(x => x.Status == Helpers.CarStatus.Available.ToString()).ToList();
            return PartialView("_Cars", cars);
        }
        [HttpGet]
        [Authorize]
        public IActionResult AddNewCar()
        {
            ViewBag.Models = carMiddleWare.GetModels();
            ViewBag.GearBox = carMiddleWare.GetGearBox().Select(x => new { Name = x });
            return View(new Models.NewCar());
        }

        [HttpPost]
        [Authorize]
        public IActionResult AddNewCar(Models.NewCar car)
        {
            if (ModelState.IsValid)
            {
                if (carMiddleWare.Add(car))
                {
                    return RedirectToAction("Index");
                }

            }
            ViewBag.GearBox = carMiddleWare.GetGearBox().Select(x => new { Name = x });
            ViewBag.Models = carMiddleWare.GetModels();
            return View(car);
        }

        [HttpPost]
        public async Task<IActionResult> ImageUpload(Models.NewCar model)
        {
            try
            {

                if (model.FormFile != null && model.FormFile.Length > 0)
                {
                    //var fileName = "temp" + Path.GetExtension(Path.GetFileName(model.FormFile.FileName));
                    var fileName = "temp.jpg";
                    var filePath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot\\images\\cars", fileName);
                    if (System.IO.File.Exists(filePath))
                    {
                        System.IO.File.Delete(filePath);
                    }
                    using (var fileSteam = new FileStream(filePath, FileMode.Create))
                    {
                        await model.FormFile.CopyToAsync(fileSteam);
                    }
                    model.Image = fileName;
                }
                ViewBag.GearBox = carMiddleWare.GetGearBox().Select(x => new { Name = x });
                ViewBag.Models = carMiddleWare.GetModels();
                foreach (var modelValue in ModelState.Values)
                {
                    modelValue.Errors.Clear();
                }
                if (model.Id == null || model.Id == 0)
                {
                    return View("AddNewCar", model);
                }
                else
                {
                    return View("Edit", model);
                }




            }
            catch (Exception exception)
            {
                return BadRequest();
            }

        }
        [HttpGet]
        public IActionResult Edit(int id)
        {
            ViewBag.GearBox = carMiddleWare.GetGearBox().Select(x => new { Name = x });
            ViewBag.Models = carMiddleWare.GetModels();
            var model = carMiddleWare.GetById(id);
            return View("Edit", model);
        }
        [HttpPost]
        public IActionResult Edit(Models.NewCar car)
        {
            if (ModelState.IsValid)
            {
                if (carMiddleWare.Edit(car))
                {
                    return RedirectToAction("Index");
                }
            }
            ViewBag.GearBox = carMiddleWare.GetGearBox().Select(x => new { Name = x });
            ViewBag.Models = carMiddleWare.GetModels();
            return View("Edit", car);
        }
        [HttpGet]
        public IActionResult Delete(int id)
        {
            carMiddleWare.DeleteById(id);
            return Json(id);
        }
        [HttpGet]
        [Authorize]
        public IActionResult ViewDeal(int id)
        {
            ViewBag.TypesOfPayment = new List<Models.Item>() {
                new Models.Item(){ Name = "Card" },
                new Models.Item(){ Name = "Cash" }
            };
            var model = carMiddleWare.GetById(id);
            return View(carMiddleWare.CastNewCarToViewDeal(model));
        }
        [HttpPost]
        [Authorize]
        public IActionResult ViewDeal(Models.ViewDeal model)
        {
            if (ModelState.IsValid)
            {
                carMiddleWare.Rezerv(model);

                return RedirectToAction("History", "Account");
            }
            ViewBag.TypesOfPayment = new List<Models.Item>() {
                new Models.Item(){ Name = "Card" },
                new Models.Item(){ Name = "Cash" }
            };
            return View(model);
        }
        [HttpGet]
        [Authorize]
        public IActionResult AllRezervations()
        {
            return View();
        }

        [HttpPost]
        [Authorize(Roles = "admin")]
        public IActionResult GetAllRezervations()
        {

            var length = Convert.ToInt32(Request.Form["length"]);
            var start = Convert.ToInt32(Request.Form["start"]);
            var col = Request.Form[$"columns[{Request.Form["order[0][column]"]}][name]"];
            var search = Request.Form["search[value]"];
            var dir = Request.Form["order[0][dir]"];
            var rezs = carMiddleWare.GetAllRezervations(col, dir, search).ToList();
            return Json(new
            {
                data = rezs.Skip(start).Take(length),
                recordsTotal = rezs.Count(),
                recordsFiltered = rezs.Count()
            });
        }
        [HttpPost]
        public IActionResult Charge(Object obj)
        {
            return RedirectToAction("AllRezervations");
        }
        [HttpGet]
        [Authorize(Roles = "admin")]
        public IActionResult ViewRezervationDetails(int id)
        {
            var rez = carMiddleWare.GetRezervationDetailsById(id);
            return PartialView("_ViewRezervationDetails", rez);
        }

        [HttpPost]
        [Authorize(Roles = "admin")]
        public IActionResult ViewRezervationDetails(Models.ViewRezervationDetails model)
        {
            if (ModelState.IsValid)
            {
                carMiddleWare.CarReturned(model);
                return RedirectToAction("AllRezervations");
            }
            return PartialView("_ViewRezervationDetails", model);
        }

    }
}