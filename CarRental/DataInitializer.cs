﻿using Microsoft.AspNetCore.Identity;
using PersitanceDb;
using System;

using System.Linq;

namespace CarRental
{
    internal class DataInitializer
    {
        private UserManager<PersitanceDb.Entities.User> userManager;
        private DatabaseContext db;
        public DataInitializer()
        {
        }
        private void CreateRoles()
        {
            db.Roles.AddRange(new IdentityRole()
            {
                Name = "admin"
            }, new IdentityRole()
            {
                Name = "user"
            });

            db.SaveChanges();
        }
        private void CreateUsers()
        {
            for (int i = 1; i < 15; i++)
            {
                var userRole = db.Roles.FirstOrDefault(x => x.Name == "user");
                var newUser = new PersitanceDb.Entities.User()
                {
                    Address = "Address",
                    Email = $"user{i}@gmail.com",
                    EmailConfirmed = false,
                    LastName = $"LastName{i}",
                    FirstName = $"FirstName{i}",
                    UserName = $"user{i}@gmail.com",
                };

                var result = userManager.CreateAsync(newUser, $"pass{i}").Result;

                if (result.Succeeded)
                {
                    db.Entry(newUser).Reload();
                    var roleAssigned = new IdentityUserRole<string>()
                    {
                        UserId = newUser.Id,
                        RoleId = userRole.Id,
                    };
                    db.UserRoles.Add(roleAssigned);
                    db.SaveChanges();
                }
            }


            var userRol = db.Roles.FirstOrDefault(x => x.Name == "user");
            var adminRol = db.Roles.FirstOrDefault(x => x.Name == "admin");
            var user = new PersitanceDb.Entities.User()
            {
                Address = "Address",
                Email = "marcupalii@gmail.com",
                EmailConfirmed = false,
                FirstName = "admin",
                LastName = "admin",
                UserName = "marcupalii@gmail.com",
            };

            var res = userManager.CreateAsync(user, "admin").Result;

            if (res.Succeeded)
            {
                db.Entry(user).Reload();
                var roleAssigned1 = new IdentityUserRole<string>()
                {
                    UserId = user.Id,
                    RoleId = userRol.Id
                };
                var roleAssigned2 = new IdentityUserRole<string>()
                {
                    UserId = user.Id,
                    RoleId = adminRol.Id
                };
                db.UserRoles.AddRange(roleAssigned1, roleAssigned2);
                db.SaveChanges();
            }

        }
        private void CreateCars()
        {
            using (var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    var newMarca = new PersitanceDb.Entities.CarModels()
                    {
                        Name = "Dacia"
                    };
                    db.CarModels.Add(newMarca);
                    db.SaveChanges();
                    db.Entry(newMarca).Reload();

                    var newCar = new PersitanceDb.Entities.Car()
                    {
                        Name = "Logan",
                        CarModel = newMarca
                    };
                    db.Cars.Add(newCar);
                    newMarca.Car.Add(newCar);
                    db.SaveChanges();
                    db.Entry(newCar).Reload();

                    var newCharacteristic = new PersitanceDb.Entities.CarCharacteristics()
                    {
                        IdCar = newCar.Id,
                        Color = "Grey",
                        PricePerDay = 150.0f,
                        CountDoors = 4,
                        CountSeats = 5,
                        CountLargeBags = 2,
                        CountSmallBags = 3,
                        HasAirConditioning = true,
                        Gearbox = "Manual",
                        Status = Helpers.CarStatus.Available.ToString(),
                        Km = 50000.2f,
                        Img = "1.jpg",
                        TotalVotes = 0,
                        TotalVotesSum = 0
                    };
                    db.CarCharacteristics.Add(newCharacteristic);
                    db.SaveChanges();
                    // -------------------------------------------------------


                    var newMarca1 = new PersitanceDb.Entities.CarModels()
                    {
                        Name = "Renault"
                    };
                    db.CarModels.Add(newMarca1);
                    db.SaveChanges();
                    db.Entry(newMarca1).Reload();

                    var newCar1 = new PersitanceDb.Entities.Car()
                    {
                        Name = "Fluence",
                        CarModel = newMarca1
                    };
                    db.Cars.Add(newCar1);
                    newMarca1.Car.Add(newCar1);
                    db.SaveChanges();
                    db.Entry(newCar1).Reload();

                    var newCharacteristic1 = new PersitanceDb.Entities.CarCharacteristics()
                    {
                        IdCar = newCar1.Id,
                        Color = "White",
                        PricePerDay = 200.0f,
                        CountDoors = 4,
                        CountSeats = 5,
                        CountLargeBags = 1,
                        CountSmallBags = 2,
                        HasAirConditioning = true,
                        Gearbox = "Automatic ",
                        Status = Helpers.CarStatus.Available.ToString(),
                        Km = 1000.2f,
                        Img = "2.jpg",
                        TotalVotes = 0,
                        TotalVotesSum = 0
                    };
                    db.CarCharacteristics.Add(newCharacteristic1);
                    db.SaveChanges();

                    // -------------------------------------------------------


                    var newMarca2 = new PersitanceDb.Entities.CarModels()
                    {
                        Name = "Smart"
                    };
                    db.CarModels.Add(newMarca2);
                    db.SaveChanges();
                    db.Entry(newMarca2).Reload();

                    var newCar2 = new PersitanceDb.Entities.Car()
                    {
                        Name = "Fortwo Pure",
                        CarModel = newMarca2
                    };
                    db.Cars.Add(newCar2);
                    newMarca2.Car.Add(newCar2);
                    db.SaveChanges();
                    db.Entry(newCar2).Reload();

                    var newCharacteristic2 = new PersitanceDb.Entities.CarCharacteristics()
                    {
                        IdCar = newCar2.Id,
                        Color = "White",
                        PricePerDay = 220.0f,
                        CountDoors = 2,
                        CountSeats = 2,
                        CountLargeBags = 0,
                        CountSmallBags = 3,
                        HasAirConditioning = true,
                        Gearbox = "Automatic",
                        Status = Helpers.CarStatus.Available.ToString(),
                        Km = 2000.2f,
                        Img = "3.jpg",
                        TotalVotes = 0,
                        TotalVotesSum = 0
                    };
                    db.CarCharacteristics.Add(newCharacteristic2);
                    db.SaveChanges();
                    transaction.Commit();
                }
                catch (Exception exception)
                {
                    transaction.Rollback();
                }
            }


        }
        public void InitializeDataAsync(UserManager<PersitanceDb.Entities.User> userManager, DatabaseContext db)
        {
            this.userManager = userManager;
            this.db = db;
            try
            {
                if (db.Roles.Count() == 0)
                {
                    CreateRoles();
                }
                if (db.User.Count() == 0)
                {
                    CreateUsers();
                }
                if (db.Cars.Count() == 0)
                {
                    CreateCars();
                }
            }
            catch (Exception exception)
            {
            }
        }

    }
}
