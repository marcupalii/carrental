﻿
using System.ComponentModel.DataAnnotations;

namespace CarRental.Models
{
    public class ResetPassword
    {
        [Required(ErrorMessage = "Email Address is required!")]
        [EmailAddress(ErrorMessage = "Enter a valid email!")]
        [Display(Name = "Email Address: ")]
        public string Email { get; set; }
    }
}
