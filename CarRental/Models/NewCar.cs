﻿using Microsoft.AspNetCore.Http;
using System;
using System.ComponentModel.DataAnnotations;

namespace CarRental.Models
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = false)]
    public class ValidateImage : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var formImg = validationContext.ObjectType.GetProperty("FormFile").GetValue(validationContext.ObjectInstance, null);

            string img = (string)validationContext.ObjectType.GetProperty("Image").GetValue(validationContext.ObjectInstance, null);

            //check at least one has a value
            if (formImg == null && string.IsNullOrEmpty(img))
                return new ValidationResult("Field is required!!");

            return ValidationResult.Success;
        }
    }
    public class NewCar
    {
        [Required(ErrorMessage = "Field Model is required!")]
        [Display(Name = "Model: ")]
        public int IdCarModel { get; set; }

        [Required(ErrorMessage = "Field Name is required!")]
        [Display(Name = "Name: ")]
        public string Name { get; set; }
        [Required(ErrorMessage = "Field Color is required!")]
        [Display(Name = "Color: ")]
        public string Color { get; set; }
        [Required(ErrorMessage = "Field Price per day is required!")]
        [Display(Name = "Price Per Day: ")]
        public float PricePerDay { get; set; }
        [Required(ErrorMessage = "Field Number of doors is required!")]
        [Display(Name = "Number of doors: ")]
        public int CountDoors { get; set; }
        [Required(ErrorMessage = "Field Number of seats is required!")]
        [Display(Name = "Number of seats: ")]
        public int CountSeats { get; set; }
        [Required(ErrorMessage = "Field Large bags is required!")]
        [Display(Name = "Large bags: ")]
        public int CountLargeBags { get; set; }
        [Required(ErrorMessage = "Field Small bags is required!")]
        [Display(Name = "Small bags: ")]
        public int CountSmallBags { get; set; }
        [Display(Name = "Air conditioning: ")]
        public bool HasAirConditioning { get; set; }
        [Required(ErrorMessage = "Field Gearbox is required!")]
        [Display(Name = "Gearbox: ")]
        public string Gearbox { get; set; }
        [Required(ErrorMessage = "Field Status is required!")]
        [Display(Name = "Status: ")]
        public string Status { get; set; }
        [Required(ErrorMessage = "Field Km is required!")]
        [Display(Name = "Km: ")]
        public float Km { get; set; }

        [ValidateImage]
        [Display(Name = "Image: ")]
        public IFormFile FormFile { get; set; }
        public int Id { get; set; }

        public string ModelName { get; set; }
        [ValidateImage]
        public string Image { get; set; }
    }
}
