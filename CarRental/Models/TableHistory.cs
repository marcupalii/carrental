﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarRental.Models
{
    public class TableHistory
    {
        public int IdRezervation { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string ModelName { get; set; }
        public string CarName { get; set; }
        public string Status { get; set; }
        public string TypeOfPayment { get; set; }
    }
}
