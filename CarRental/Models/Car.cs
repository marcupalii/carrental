﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarRental.Models
{
    public class Car
    {
        public int IdCar { get; set; }
        public int IdCarModel { get; set; }
        public string Name { get; set; }
        public string ModelName { get; set; }
        public string Color { get; set; }
        public float PricePerDay { get; set; }
        public int CountDoors { get; set; }
        public int CountSeats { get; set; }
        public int CountLargeBags { get; set; }
        public int CountSmallBags { get; set; }
        public bool HasAirConditioning { get; set; }
        public string GearBox { get; set; }
        public string Status { get; set; }
        public float Km { get; set; }
        public string Image { get; set; }
        public float Rating { get; set; }
    }
}
