﻿
using System.Collections.Generic;

namespace CarRental.Models
{
    public class Item
    {
        public string Name { get; set; }
        public bool Selected { get; set; }
    }
    public class CarFilters
    {
        public CarFilters()
        {
            Models = new List<Item>();
            Seats = new List<Item>();
            Doors = new List<Item>();
            Colors = new List<Item>();
            CountLargeBags = new List<Item>();
            CountSmallBags = new List<Item>();
            GearTypes = new List<Item>();
        }
        public List<Item> Models { get; set; }
        public List<Item> Seats { get; set; }
        public List<Item> Doors { get; set; }
        public List<Item> Colors { get; set; }
        public List<Item> CountLargeBags { get; set; }
        public List<Item> CountSmallBags { get; set; }
        public List<Item> GearTypes { get; set; }

    }
}
