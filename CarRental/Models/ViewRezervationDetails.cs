﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarRental.Models
{
    public class ViewRezervationDetails : ViewDeal
    {
        public int IdRezervation { get; set; }
        public string UserFirstName { get; set; }
        public string UserLastName { get; set; }
        public bool Voted { get; set; }
    }
}
