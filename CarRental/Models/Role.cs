﻿
using System.ComponentModel.DataAnnotations;

namespace CarRental.Models
{
    public class Role
    {
        [Required]
        public string Id { get; set; }
        public string Name { get; set; }
    }
}
