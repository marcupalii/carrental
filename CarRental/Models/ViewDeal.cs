﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CarRental.Models
{
    public class ViewDeal : NewCar
    {
        [Required(ErrorMessage = "Field Start date is required!")]
        [Display(Name = "Start date: ")]
        public DateTime StartDate { get; set; }
        [Required(ErrorMessage = "Field End date is required!")]
        [Display(Name = "End date: ")]
        public DateTime EndDate { get; set; }
        [Required(ErrorMessage = "Field Payment is required!")]
        [Display(Name = "Payment: ")]
        public string TypeOfPayment { get; set; }
        public string IdUser { get; set; }
        public float Rating { get; set; }
    }
}
