﻿
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace PersitanceDb.Entities
{
    public class CarModels
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public virtual List<Car> Car { get; set; }
    }
}
