﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PersitanceDb.Entities
{
    public class Rezervation
    {
        [Key]
        public int Id { get; set; }

        [ForeignKey("Car")]
        public int IdCar { get; set; }
        public virtual Car Car { get; set; }
        [ForeignKey("User")]
        public string IdUser { get; set; }
        public virtual User User { get; set; }
        public DateTime DateStart { get; set; }
        public DateTime DateEnd { get; set; }
        public string Status { get; set; }
        public string TypeOfPayment { get; set; }
        public bool Voted { get; set; }
    }
}
