﻿
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PersitanceDb.Entities
{
    public class Car
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        //[ForeignKey("CarModels")]
        //public int IdCarModel { get; set; }
        public virtual CarModels CarModel { get; set; }
    }
}
