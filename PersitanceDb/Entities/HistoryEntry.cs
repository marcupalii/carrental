﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PersitanceDb.Entities
{
    public class HistoryEntry
    {
        [Key]
        public int Id { get; set; }
        [ForeignKey("UserHistory")]
        public int IdUserHistory { get; set; }
        public virtual UserHistory UserHistory { get; set; }
        public virtual Car Car { get; set; }
        public DateTime DateStart { get; set; }
        public DateTime DateEnd { get; set; }
    }
}
