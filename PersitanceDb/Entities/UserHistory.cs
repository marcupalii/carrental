﻿
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PersitanceDb.Entities
{
    public class UserHistory
    {
        [Key]
        public int Id { get; set; }
        [ForeignKey("User")]
        public string IdUser { get; set; }
        public virtual User User { get; set; }
        public virtual List<HistoryEntry> HistoryEntry { get; set; }
    }
}
