﻿
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PersitanceDb.Entities
{
    public class CarCharacteristics
    {
        [Key]
        public int Id { get; set; }
        [ForeignKey("Car")]
        public int IdCar { get; set; }
        public virtual Car Car { get; set; }
        public string Color { get; set; }
        public float PricePerDay { get; set; }
        public int CountDoors { get; set; }
        public int CountSeats { get; set; }
        public int CountLargeBags { get; set; }
        public int CountSmallBags { get; set; }
        public bool HasAirConditioning { get; set; }
        public string Gearbox { get; set; }
        public string Status { get; set; }
        public float Km { get; set; }
        public string Img { get; set; }
        public float TotalVotes { get; set; }
        public float TotalVotesSum { get; set; }
    }
}
